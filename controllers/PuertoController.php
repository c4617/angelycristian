<?php

namespace app\controllers;

use Yii;
use app\models\Puerto;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PuertoController implements the CRUD actions for Puerto model.
 */
class PuertoController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Puerto models.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Puerto::find(),
                    /*
                      'pagination' => [
                      'pageSize' => 50
                      ],
                      'sort' => [
                      'defaultOrder' => [
                      'nompuerto' => SORT_DESC,
                      ]
                      ],
                     */
            ]);

            return $this->render('index', [
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Puerto model.
     * @param string $nompuerto Nompuerto
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($nompuerto) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($nompuerto),
            ]);
        }
    }

    /**
     * Creates a new Puerto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = new Puerto();

            if ($this->request->isPost) {
                if ($model->load($this->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'nompuerto' => $model->nompuerto]);
                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Puerto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nompuerto Nompuerto
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nompuerto) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = $this->findModel($nompuerto);

            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'nompuerto' => $model->nompuerto]);
            }

            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Puerto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nompuerto Nompuerto
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nompuerto) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $this->findModel($nompuerto)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Puerto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nompuerto Nompuerto
     * @return Puerto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nompuerto) {
        if (($model = Puerto::findOne(['nompuerto' => $nompuerto])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
}

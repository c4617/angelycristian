<?php

namespace app\controllers;

use Yii;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
/**
 * CiclistaController implements the CRUD actions for Ciclista model.
 */
class CiclistaController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Ciclista models.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find(),
                    /*
                      'pagination' => [
                      'pageSize' => 50
                      ],
                      'sort' => [
                      'defaultOrder' => [
                      'dorsal' => SORT_DESC,
                      ]
                      ],
                     */
            ]);

            return $this->render('index', [
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Ciclista model.
     * @param int $dorsal Dorsal
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($dorsal) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($dorsal),
            ]);
        }
    }

    /**
     * Creates a new Ciclista model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = new Ciclista();

            if ($this->request->isPost) {
                if ($model->load($this->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'dorsal' => $model->dorsal]);
                }
            } else {
                $model->loadDefaultValues();
            }


            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Ciclista model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $dorsal Dorsal
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dorsal) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = $this->findModel($dorsal);

            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'dorsal' => $model->dorsal]);
            }


            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ciclista model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $dorsal Dorsal
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($dorsal) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $this->findModel($dorsal)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Ciclista model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $dorsal Dorsal
     * @return Ciclista the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($dorsal) {
        if (($model = Ciclista::findOne(['dorsal' => $dorsal])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

public function actionMsw(){
    
        $dataProvider = new SqlDataProvider(['sql'=>'SELECT nombre, dorsal, edad, nomequipo FROM ciclista c WHERE c.dorsal =
                                                    (SELECT dorsal FROM etapa
                                                     GROUP BY dorsal
                                                     HAVING COUNT(*) = (SELECT MAX(c1.etapas_ganadas) FROM (
                                                     SELECT dorsal, COUNT(*) AS etapas_ganadas FROM etapa
                                                     GROUP BY dorsal
                                                     )c1))'
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nombre','dorsal','edad','nomequipo'],
            "titulo"=>"Most stages won Award",
        ]);
     }
public function actionMmw(){
    
      $dataProvider = new SqlDataProvider(['sql'=>'SELECT nombre, dorsal, edad, nomequipo FROM ciclista c WHERE dorsal =
                                                  (SELECT dorsal FROM lleva
                                                   GROUP BY dorsal
                                                   HAVING COUNT(*) =(SELECT MAX(Total) FROM (SELECT dorsal, COUNT(*) AS Total FROM lleva l GROUP BY l.dorsal) AS victorias))'
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nombre','dorsal','edad','nomequipo'],
            "titulo"=>"Most stages won Award",
        ]); 
}
public function actionMpw(){
    
      $dataProvider = new SqlDataProvider(['sql'=>'SELECT nombre, dorsal, edad, nomequipo FROM ciclista c WHERE dorsal =
                                                  (SELECT dorsal FROM puerto p
                                                   GROUP BY dorsal
                                                   HAVING COUNT(*) =(SELECT MAX(Total) FROM (
                                                   SELECT dorsal, COUNT(*) AS Total FROM puerto 
                                                   GROUP BY dorsal) AS victorias))'
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nombre','dorsal','edad','nomequipo'],
            "titulo"=>"Most stages won Award",
        ]);
}

public function actionEme(){
    
      $dataProvider = new SqlDataProvider(['sql'=>'SELECT nomequipo Equipo,etapas_ganadas FROM 
                                            (SELECT nomequipo, COUNT()etapas_ganadas FROM etapa 
                                            INNER JOIN ciclista ON etapa.dorsal = ciclista.dorsal
                                            GROUP BY nomequipo)c1 
                                             WHERE c1.etapas_ganadas = 
                                             (SELECT MAX(c1.etapas_ganadas) FROM (SELECT nomequipo, COUNT()etapas_ganadas FROM etapa 
                                             INNER JOIN ciclista ON etapa.dorsal = ciclista.dorsal
                                             GROUP BY nomequipo)c1)'
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Equipo','etapas_ganadas'],
            "titulo"=>"Equipo con mas etapas ganadas",
        ]);
}
}

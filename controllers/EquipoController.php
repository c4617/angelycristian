<?php

namespace app\controllers;

use Yii;
use app\models\Equipo;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;

/**
 * EquipoController implements the CRUD actions for Equipo model.
 */
class EquipoController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Equipo models.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Equipo::find(),
                    /*
                      'pagination' => [
                      'pageSize' => 50
                      ],
                      'sort' => [
                      'defaultOrder' => [
                      'nomequipo' => SORT_DESC,
                      ]
                      ],
                     */
            ]);

            return $this->render('index', [
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Equipo model.
     * @param string $nomequipo Nomequipo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($nomequipo) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($nomequipo),
            ]);
        }
    }

    /**
     * Creates a new Equipo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = new Equipo();

            if ($this->request->isPost) {
                if ($model->load($this->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'nomequipo' => $model->nomequipo]);
                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Equipo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nomequipo Nomequipo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nomequipo) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = $this->findModel($nomequipo);

            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'nomequipo' => $model->nomequipo]);
            }

            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Equipo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nomequipo Nomequipo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nomequipo) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $this->findModel($nomequipo)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Equipo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nomequipo Nomequipo
     * @return Equipo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nomequipo) {
        if (($model = Equipo::findOne(['nomequipo' => $nomequipo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

public function actionMst(){
    
      $dataProvider = new SqlDataProvider(['sql'=>'SELECT nomequipo Equipo,etapas_ganadas FROM 
                                            (SELECT nomequipo, COUNT()etapas_ganadas FROM etapa 
                                            INNER JOIN ciclista ON etapa.dorsal = ciclista.dorsal
                                            GROUP BY nomequipo)c1 
                                             WHERE c1.etapas_ganadas = 
                                             (SELECT MAX(c1.etapas_ganadas) FROM (SELECT nomequipo, COUNT()etapas_ganadas FROM etapa 
                                             INNER JOIN ciclista ON etapa.dorsal = ciclista.dorsal
                                             GROUP BY nomequipo)c1)'
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Equipo','etapas_ganadas'],
            "titulo"=>"Equipo con mas etapas ganadas",
        ]);
}
public function actionMd(){
    
      $dataProvider = new SqlDataProvider(['sql'=>'SELECT nomequipo Equipo,etapas_ganadas FROM 
                                            (SELECT nomequipo, COUNT()etapas_ganadas FROM etapa 
                                            INNER JOIN ciclista ON etapa.dorsal = ciclista.dorsal
                                            GROUP BY nomequipo)c1 
                                             WHERE c1.etapas_ganadas = 
                                             (SELECT MAX(c1.etapas_ganadas) FROM (SELECT nomequipo, COUNT()etapas_ganadas FROM etapa 
                                             INNER JOIN ciclista ON etapa.dorsal = ciclista.dorsal
                                             GROUP BY nomequipo)c1)'
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['Equipo','etapas_ganadas'],
            "titulo"=>"Equipo con mas etapas ganadas",
        ]);
}
}

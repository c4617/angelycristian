<?php

namespace app\controllers;

use Yii;
use app\models\Maillot;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;

/**
 * MaillotController implements the CRUD actions for Maillot model.
 */
class MaillotController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Maillot models.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Maillot::find(),
                    /*
                      'pagination' => [
                      'pageSize' => 50
                      ],
                      'sort' => [
                      'defaultOrder' => [
                      'código' => SORT_DESC,
                      ]
                      ],
                     */
            ]);

            return $this->render('index', [
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Maillot model.
     * @param string $código Código
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($código) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($código),
            ]);
        }
    }

    /**
     * Creates a new Maillot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = new Maillot();

            if ($this->request->isPost) {
                if ($model->load($this->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'código' => $model->código]);
                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Maillot model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $código Código
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($código) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = $this->findModel($código);

            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'código' => $model->código]);
            }

            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Maillot model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $código Código
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($código) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $this->findModel($código)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Maillot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $código Código
     * @return Maillot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($código) {
        if (($model = Maillot::findOne(['código' => $código])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
public function actionMv(){
    
      $dataProvider = new SqlDataProvider(['sql'=>'SELECT código, tipo, color, premio FROM maillot m WHERE m.código=
(SELECT código FROM 
  (SELECT m.código,premio FROM maillot m) AS c1 WHERE c1.premio=
  (SELECT MAX(premio) FROM
  (SELECT m.código,premio FROM maillot m) AS c1))'
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['código','tipo','color','premio'],
            "titulo"=>"Maillot mas valioso",
        ]);
}
}

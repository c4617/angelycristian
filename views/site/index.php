<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Premios al ciclismo 2022';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white" Style="margin-bottom: 0px">
        <h1 class="display-4">SALON DE LA FAMA</h1>

        <p class="lead">Donde todos quieren estar, pero solo unos pocos están dispuestos a hacer cualquier cosa para llegar allí.</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row">
            
            <div class="card center  fame-card">
                <?= Html::img("@web/images/A_cyclist.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Ciclistas', ['site/cyclistfame'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
            <div class="card center  fame-card">
                <?= Html::img("@web/images/A_team.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Equipos', ['site/teamfame'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
            <div class="card center  fame-card">
                <?= Html::img("@web/images/A_stage.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Etapas', ['site/stagefame'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
            <div class="card center  fame-card">
                <?= Html::img("@web/images/A_maillot.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Maillots', ['site/maillotfame'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
        </div>

        <div class="jumbotron text-right bg-transparent text-white flex-shrink-1" Style="padding-bottom: 1px; margin-bottom: 0px"></div>

    </div>
</div>
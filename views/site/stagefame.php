<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'ETAPAS MÁS DIFÍCILES';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white" Style="margin-bottom: -30px">
        <h1 class="display-4">PREMIOS PARA LAS CATEGORÍAS MÁS DIFÍCILES</h1>

        <p class="lead">Si no te supone un reto, no te va a cambiar.</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row">

            <div class="card text-center  fame-card">
                <?= Html::img("@web/images/A_stage.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Etapa más larga</h5>
                    <?= Html::a('Ver', ['etapa/el'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

            <div class="card text-center  fame-card">
                <?= Html::img("@web/images/ports.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Etapa con más puertos</h5>
                    <?= Html::a('Ver', ['etapa/emp'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

            <div class="card text-center  fame-card">
                <?= Html::img("@web/images/port.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Puerto más alto</h5>
                    <?= Html::a('Ver', ['etapa/pma'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

        </div>

        <div class="jumbotron text-right bg-transparent text-white d-flex col-12" Style="justify-content: space-between; padding-bottom: 1px; margin-bottom: 0px ">

            <?= Html::a('Atrás', ['site/index'], ['class' => 'btn btn-warning']) ?>
        </div>

    </div>
</div>
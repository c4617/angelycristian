<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'TOP CYCLYSTS';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white" Style="margin-bottom: -30px">
        <h1 class="display-4">MEJORES CICLISTAS</h1>

        <p class="lead">Mantente positivo, trabaja duro, haz que suceda.</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row">

            <div class="card text-center  fame-card">
                <?= Html::img("@web/images/A_stage.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Mas etapas ganadas</h5>
                    <?= Html::a('Ver', ['ciclista/msw'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

            <div class="card text-center  fame-card">
                <?= Html::img("@web/images/A_maillot.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Mas maillots ganados</h5>
                    <?= Html::a('Ver', ['ciclista/mmw'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

            <div class="card text-center  fame-card">
                <?= Html::img("@web/images/port.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Mas puertos ganados</h5>
                    <?= Html::a('Ver', ['ciclista/mpw'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

        </div>

        <div class="jumbotron text-right bg-transparent text-white d-flex col-12" Style="justify-content: space-between; padding-bottom: 1px; margin-bottom: 0px ">

                <?= Html::a('Atras', ['site/index'], ['class' => 'btn btn-warning']) ?>
        </div>

        
    </div>
</div>
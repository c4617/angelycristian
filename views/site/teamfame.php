<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'MEJORES EQUIPOS';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white" Style="margin-bottom: -30px">
        <h1 class="display-4">MEJORES EQUIPOS</h1>

        <p class="lead">El trabajo en equipo divide la tarea y multiplica el éxito</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row" Style="justify-content: space-around">

            <div class="card text-center  fame-card col-4">
                <?= Html::img("@web/images/A_stage.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Equipo con mas etapas ganadas</h5>
                    <?= Html::a('Ver', ['equipo/mst'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

            <div class="card text-center  fame-card col-4">
                <?= Html::img("@web/images/director.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Mejor director</h5>
                    <?= Html::a('Ver', ['equipo/md'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

        </div>

        <div class="jumbotron text-right bg-transparent text-white d-flex col-12" Style="justify-content: space-between; padding-bottom: 1px; margin-bottom: 0px ">

            <?= Html::a('Atras', ['site/index'], ['class' => 'btn btn-warning']) ?>
        </div>

    </div>
</div>
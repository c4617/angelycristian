<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'MAILLOTS MÁS PRECIADOS';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white" Style="margin-bottom: -30px">
        <h1 class="display-4">PREMIOS PARA LOS MALLOTS MÁS PRECIADOS</h1>

        <p class="lead">Quizás me veas sufrir, pero nunca me verás rindiéndome.</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row" Style="justify-content: space-around">

            <div class="card text-center fame-card col-4">
                <?= Html::img("@web/images/A_maillot.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Maillot más valioso</h5>
                    <?= Html::a('Ver', ['maillot/mv'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

        </div>

        <div class="jumbotron text-right bg-transparent text-white d-flex col-12" Style="justify-content: space-between; padding-bottom: 1px; margin-bottom: 0px ">
            <?= Html::a('Atrás', ['site/index'], ['class' => 'btn btn-warning']) ?>
        </div>

    </div>
</div>
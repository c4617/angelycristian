<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Premios al ciclismo 2022';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white" Style="margin-bottom: 0px">
        <h1 class="display-4">CONTROL DE CRUDS</h1>

        <p class="lead">AquÃ­ podras crear, leer, actualizar o borrar registros de la app</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row">
            
            <div class="card center  fame-card">
                <?= Html::img("@web/images/A_cyclist.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Ciclistas', ['ciclista/index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
            <div class="card center  fame-card">
                <?= Html::img("@web/images/A_team.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Equipos', ['equipo/index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
            <div class="card center  fame-card">
                <?= Html::img("@web/images/A_stage.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Etapas', ['etapa/index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
            <div class="card center  fame-card">
                <?= Html::img("@web/images/A_maillot.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Maillots', ['maillot/index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
                        <div class="card center  fame-card">
                <?= Html::img("@web/images/port.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Puertos', ['puerto/index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
                        <div class="card center  fame-card">
                <?= Html::img("@web/images/carries.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Llevan', ['lleva/index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
        </div>

    </div>
</div>
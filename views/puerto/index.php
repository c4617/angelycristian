<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ports';
$this->params['breadcrumbs'][] = 'Ports';
?>
<div class="puerto-index">

    <h1>Ports</h1>

    <p>
        <?= Html::a('Create Port', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nompuerto',
            'altura',
            'categoria',
            'pendiente',
            'numetapa',
            //'dorsal',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'nompuerto' => $model->nompuerto]);
                 }
            ],
        ],
    ]); ?>


</div>

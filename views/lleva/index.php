<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View /
/ @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Llevan';
$this->params['breadcrumbs'][] = 'Llevan';
?>
<div class="lleva-index">

    <h1>LLevan</h1>

    <p>
        <?= Html::a('Crear', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dorsal',
            'numetapa',
            'código',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'numetapa' => $model->numetapa, 'código' => $model->código]);
                 }
            ],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View /
/ @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Etapas';
$this->params['breadcrumbs'][] = 'Etapas';
?>
<div class="etapa-index">

    <h1>Etapas</h1>

    <p>
        <?= Html::a('Crear', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'numetapa',
            'kms',
            'salida',
            'llegada',
            'dorsal',
            'nompuerto',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'numetapa' => $model->numetapa]);
                 }
            ],
        ],
    ]); ?>


</div>

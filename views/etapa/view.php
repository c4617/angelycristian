<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Etapa */

$this->title = $model->numetapa;
$this->params['breadcrumbs'][] = ['label' => 'Etapas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="etapa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'numetapa' => $model->numetapa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'numetapa' => $model->numetapa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro que quieres borrar este objeto?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'numetapa',
            'kms',
            'salida',
            'llegada',
            'dorsal',
            'nompuerto',
        ],
    ]) ?>

</div>
